from tree_sitter import Language, Parser

Language.build_library(
  # Store the library in the `build` directory
  'ts-languages.so',

  # Include one or more languages
  [
    'tree-sitter-bash',
    'tree-sitter-c',
    'tree-sitter-cpp',
    'tree-sitter-go',
    'tree-sitter-haskell',
    'tree-sitter-javascript',
    'tree-sitter-python',
    'tree-sitter-rust'
  ]
)
